import java.io.*;

class SwapCase {
	String original_data;
	String swap_data = "";
	String swapCase(String data) {
		this.original_data = data;
		for (int i = 0; i < original_data.length(); i++) {
            String store = original_data.charAt(i) + "";
            if (original_data.charAt(i) >= 97 && original_data.charAt(i) <= 122) {
                    store = store.toUpperCase();
                    swap_data = swap_data + store;
           }else if (original_data.charAt(i) >= 65 && original_data.charAt(i) <= 90) {
                    store = store.toLowerCase();    
                    swap_data = swap_data + store;
           }else {
                    swap_data = swap_data + store;
           }
        }
        return swap_data;
        }
        }

class FileReaderDemo { 
	public static void main(String[] args) throws IOException {
		SwapCase sc = new SwapCase();
		String file_name = args[0];
		String data = "";
		char character;
		int value;
		try {
		FileReader fr = new FileReader(file_name);
		while((value = fr.read()) != -1) {
				character = (char) value;
				data = data + character;
				}
				fr.close();
	   }catch(FileNotFoundException e) {
        System.out.println("File Not Found Exception");
	   }catch(Exception e) {
		e.printStackTrace();
	   }
		System.out.println(sc.swapCase(data));
		}
	}
	
